import { Status } from "./Enums";
import { Title, Layer } from "./Types";

export interface Menu {
    title: Title,
    status: Status,
    index: number
}

export interface Day extends Array<Menu> {
    [index: number]: Menu
}

export interface Days extends Array<Day> {
    [index: number]: Day
}

export interface TrainingSet {
    input: Layer,
    output: Layer
}

export interface TrainingData {
    input_size: number,
    output_size: number,
    sets: Array<TrainingSet>
}

export interface Week {
    week: number,
    year: number
}