import _ from 'lodash'
import { Days, TrainingSet, TrainingData } from '../Interfaces';

export default function ({ lexicon, days }: { lexicon: string[], days: Days }): TrainingData {

    const trainingdata: TrainingData = { 
        input_size: lexicon.length * 3,
        output_size: 3,
        sets: []
    }

    for(const day of days) {
        const output: Array<(1|0)> = day.map(menu => menu.status == '2' ? 1 : 0)

        if(output.filter(out => out == 1).length == 1) {
            const input: (0|1)[] = _.flatten(day.map(menu => lexicon.map(word => menu.title.toLowerCase().replace(/ß/g, 'ß').includes(word.toLowerCase().replace(/ß/g, 'ss')) ? 1 : 0)))

            trainingdata.sets.push({
                input,
                output
            })
        }
    }

    return trainingdata
}