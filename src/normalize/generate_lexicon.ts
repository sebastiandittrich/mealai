import Tokenizer from 'wink-tokenizer'
import { Days } from '../Interfaces';

const tokenizer = Tokenizer()

export default function(days: Days): string[] {
    const words: Array<{ value: string, count: number, tag: string }> = []

    for(const day of days) {
        for(const meal of day) {
            if(meal.title) {
                const newwords = tokenizer.tokenize(meal.title).filter(word => word.tag == 'word')
                for(const word of newwords) {
                    const found = words.filter(found => found.value == word.value)
                    if(found.length > 0) {
                        found[0].count++
                    } else {
                        words.push({ ...word, count: 1 })
                    }
                }
            }
        }
    }

    return words.filter(word => word.value.length > 3).filter(word => word.value.match(/^[A-Z]/)).filter(word => word.count > 2).map(word => word.value)
}