import dayifier from "./dayifier";
import generate_lexicon from './generate_lexicon'
import generate_training from './generate_training'
import fs from 'fs'
import analyse from "./analyse";

const weeks = JSON.parse(fs.readFileSync('./sources/data.json').toString())

const days = dayifier({ weeks: (<any>weeks) })
// const lexicon = generate_lexicon(days)
const lexicon = JSON.parse(fs.readFileSync('./normalized/lexicon.json').toString())
const training = generate_training({ days, lexicon })

analyse({ training })

// for(const train of training) {
//     if(train.input.length != 1527) {
//         console.log('input wrong')
//     }
//     if(train.output.length != 3 || train.output.filter(out => out == 1).length != 1) {
//         console.log('output wrong', train.output)
//     }
// }

// fs.writeFileSync('./normalized/lexicon.json', JSON.stringify(lexicon))
fs.writeFileSync('./normalized/training.json', JSON.stringify(training))