import { TrainingData, TrainingSet } from "../Interfaces";
import _ from 'lodash'

export default function({ training }: { training: TrainingData }) {
    const total_training_sets = training.sets.length
    let input_lengths_wrong = 0
    let output_lengths_wrong = 0

    for(const set of training.sets) {
        if(training.input_size != set.input.length) input_lengths_wrong++
        if(training.output_size != set.output.length) output_lengths_wrong++
    }

    const total_words_matching = training.sets.reduce((total, set) => total + set.input.filter(input => input == 1).length, 0)
    const average_words_per_menu = total_words_matching / (training.sets.length * 3)
    const sort_by_word_count = _.orderBy(training.sets, (set: TrainingSet) => set.input.filter(input => input == 1).length / 3).map(set => set.input.filter(input => input == 1).length / 3)
    const min_words = sort_by_word_count[0]
    const max_words = sort_by_word_count[sort_by_word_count.length -1]

    console.log('Total training sets:', total_training_sets)
    if(input_lengths_wrong > 0) {
        console.log('!!! INPUT LENGTHS WRONG !!!')
    }
    if(output_lengths_wrong > 0) {
        console.log('!!! OUTPUT LENGTHS WRONG !!!')
    }
    console.log('Average words per menu:', average_words_per_menu)
    console.log('Minimum words per menu:', min_words)
    console.log('Maximum words per menu:', max_words)
    console.log('Input length:', training.input_size)
    console.log('Output length:', training.output_size)
}