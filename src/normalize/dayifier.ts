import { Days, Day } from '../Interfaces';
import _ from 'lodash'
import { Status } from '../Enums';
import { Title } from '../Types';

function isValidDay(day: Day): boolean {
    let menus_available: boolean = false
    let all_vacations = true
    for(const menu of day) {
        if(menu.title) {
            menus_available = true
            if(!menu.title.match(/.*Ferien.*/gi)) {
                all_vacations = false
            }
        }
    }

    return menus_available && !all_vacations
}

export default function({ weeks }: { weeks: any }): Days {
    const days: Days = []

    for(const week of weeks) {
        const weekdays = _.groupBy(week.list, 'index')
        for(let daynumber in weekdays) {
            const day = weekdays[daynumber]
            if(isValidDay(day)) {
                days.push(day.map(menu => ({
                    title: <Title>menu.title || '',
                    status: <Status>menu.status,
                    index: <Number>day.index
                })))
            }
        }
    }

    return days
}