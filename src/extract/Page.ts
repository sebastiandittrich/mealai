import puppeteer, { Browser } from "puppeteer";
import moment from '../moment';
import { Week } from "../Interfaces";
import { Moment } from "moment";

export class Page {
    browser: Browser;
    page: puppeteer.Page;
    menuframe: puppeteer.Frame;
    week: moment.Moment = moment();

    constructor() {
        
    }

    async startBrowser() {
        this.browser = await puppeteer.launch({ headless: false, args: [ '--no-sandbox' ] })
    }

    async openPage() {
        this.page = await this.browser.newPage()
        
        await this.page.goto('https://www.essen-bei-sodexo.de/')
    }

    async login({ kdnr, pin }: { kdnr: string, pin: string }) {
        await (await this.page.$('input#kdnr')).type(kdnr)
        await (await this.page.$('input#pin')).type(pin)
        await (await this.page.$('input[name=login]')).click()
        await this.page.waitForSelector('button.ngdialog-button.ngdialog-button-primary')
        await (await this.page.$('button.ngdialog-button.ngdialog-button-primary')).click()

        // Wait for iframe to appear
        await this.page.waitForSelector('#ibs')
        await new Promise(resolve => this.page.on('framenavigated', () => resolve()))
        this.menuframe = this.page.frames().find(frame => frame.name() == 'null')
        await this.menuframe.waitForSelector('.menu')
    }

    static async make({ credentials }: { credentials: { kdnr: string, pin: string } }) {
        const page = new this()
        await page.startBrowser()
        await page.openPage()
        await page.login(credentials)

        return page
    }

    async close() {
        return await this.browser.close()
    }

    async extract() {
        const week = parseInt((await this.menuframe.$eval('.calendar-week', (week: any): string => week.innerText)).match(/[0123456789]+/g)[0])
        const list = []

        const menus = await this.menuframe.$$('.order-menu-node, .order-menu-node-disabled')
        for(const menu of menus) {

            const title = await menu.$eval('.order-menu-node-name, .order-menu-node-name-disabled', (name: any) => name.title).catch(() => '')
            const status = await menu.$eval('.menuplan-checkbox', (checkbox: any) => checkbox.dataset.orderStatus).catch(() => '')
            const index = menus.indexOf(menu) % 7

            list.push({ title, status, index })
        }

        return { list, week }
    }

    async gotoWeek(week: Moment) {
        await this.menuframe.waitForSelector('.ui-datepicker-trigger')

        await (await this.menuframe.$('.ui-datepicker-trigger')).click()

        await this.menuframe.waitForSelector('.ui-datepicker-month')
        await this.menuframe.waitForSelector('.ui-datepicker-year')

        await this.menuframe.select('.ui-datepicker-month', week.month().toString())
        await this.menuframe.select('.ui-datepicker-year', week.year().toString())

        // const days = await (await this.menuframe.$('.ui-datepicker-calendar')).$$eval('td', (td: any): string => td.innerText)
        const days = await (await this.menuframe.$('.ui-datepicker-calendar')).$$('td[data-month]')

        await days[week.date() - 1].click()

        await this.menuframe.waitForSelector('.loading-indicator', { visible: true })
        await this.menuframe.waitForSelector('.loading-indicator', { hidden: true })

        this.week = week
    }

    async nextWeek() {
        return await this.gotoWeek(this.week.add(1, 'week'))
    }

    async previousWeek() {
        return await this.gotoWeek(this.week.subtract(1, 'week'))
    }
}