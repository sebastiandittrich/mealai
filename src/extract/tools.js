function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function crawl() {
	const week = parseInt($('.calendar-week').first().text().match(/[0123456789]+/g)[0])
	const list = []
	
	if(week < lastweek) {
		currentyear++
	}

	const year = currentyear

	$('.order-menu-node, .order-menu-node-disabled')
		.each(function() {
			list.push({
				title: $(this).find('.order-menu-node-name, .order-menu-node-name-disabled').prop('title'),
				status: $(this).find('.menuplan-checkbox').attr('data-order-status'),
				index: $(this).index()
			})
		})


	lastweek = week

	return { list, year, week }
}

function nextpage() {
	$('.week-btn-icon-right').click()
}

// start at 2014, KW 36

crawllist = []
lasthash = null
goon = true
currentyear = 2014
lastweek = 35


function crawlhash(crawl) {
	return crawl.year + crawl.week + crawl.list.reduce((all, add) => all + add, '')
}

function autocrawl() {
	const lastcrawl = crawl()

	console.log('called')

	if(lasthash == crawlhash(lastcrawl)) {
		console.log('calling again later...')
		setTimeout(autocrawl, 500)
	} else {
		console.log('crawling now!')
		lasthash = crawlhash(lastcrawl)

		crawllist.push(lastcrawl)
		if(crawllist.length)
		nextpage()
		if(goon) {
			autocrawl()
		} else {
			download('data.json', JSON.stringify(crawllist))
		}
	}
}

// Use with crawl(); nextpage()
