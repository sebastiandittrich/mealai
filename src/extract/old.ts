import { Page } from "./Page";
import { Moment } from "moment";
import moment from '../moment'

export default async function({ from = moment("2014-08-17T00:00:59.628") }: { from?: Moment } = {}) {
    const page = await Page.make({ credentials: { kdnr: '302238878', pin: '2961' } })

    await page.gotoWeek(from)

    const list = []

    list.push(await page.extract())

    while(moment().diff(page.week, 'week') > 0) {
        await page.nextWeek()
        list.push(await page.extract())
    }

    page.close()

    return list
}