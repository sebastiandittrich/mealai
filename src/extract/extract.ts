import { Frame } from "puppeteer";

export default async function extract(frame: Frame) {
	const week = parseInt((await frame.$eval('.calendar-week', (week: any): string => week.innerText)).match(/[0123456789]+/g)[0])
	const list = []

    const menus = await frame.$$('.order-menu-node, .order-menu-node-disabled')
    for(const menu of menus) {

        const title = await menu.$eval('.order-menu-node-name, .order-menu-node-name-disabled', (name: any) => name.title).catch(() => '')
        const status = await menu.$eval('.menuplan-checkbox', (checkbox: any) => checkbox.dataset.orderStatus).catch(() => '')
        const index = menus.indexOf(menu) % 7

        list.push({ title, status, index })
    }

	return { list, week }
}