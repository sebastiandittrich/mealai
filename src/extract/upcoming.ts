import { Page } from "./Page";
import moment from '../moment'

export default async function({ count }: { count: number }) {
    const page = await Page.make({ credentials: { kdnr: '302238878', pin: '2961' } })

    const list = []

    list.push(await page.extract())

    for(let i = 0; i < count; i++) {
        await page.nextWeek()
        list.push(await page.extract())
    }

    page.close()

    return list
}