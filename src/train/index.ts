import train from './train'
import fs from 'fs'
import { TrainingData } from '../Interfaces';

const training: TrainingData = JSON.parse(fs.readFileSync('./normalized/training.json').toString())

const nn = train({ training })

console.log(nn)