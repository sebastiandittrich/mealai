import { Architect, Trainer } from 'synaptic'
import { TrainingData } from '../Interfaces'
import fs from 'fs';

export default function({ training }: { training: TrainingData }) {
    const perceptron = new Architect.Perceptron(training.input_size, 20, training.output_size)
    const trainer = new Trainer(perceptron)

    const dir = './storage/network/' + new Date().getTime() + '/'

    fs.mkdirSync(dir)

    let lowest_error = 1

    trainer.train(training.sets, {
        schedule: { every: 1, do: data => {
            console.log(data.iterations + ': ' + data.error)
            if(data.error < lowest_error) {
                console.log('    -> Saving this')
                fs.writeFileSync(dir + data.iterations + '-lowest.json', JSON.stringify(perceptron.toJSON()))
                lowest_error = data.error
            }
        }},
        rate: 0.1
    })

    return perceptron
}