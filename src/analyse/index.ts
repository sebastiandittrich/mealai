import { readFileSync } from "fs";
import { Network } from 'synaptic'
import { TrainingData } from "../Interfaces";
import _ from 'lodash'

const file = process.argv[2]

const json = JSON.parse(readFileSync(file).toString())
const training: TrainingData = JSON.parse(readFileSync('./normalized/training.json').toString())

const network = Network.fromJSON(json)

let wrong = 0

for(const set of training.sets) {
    const output: number[] = network.activate(set.input)

    const maxindex = output.indexOf(Math.max(...output))

    const expectedindex = set.output.indexOf(set.output.filter(number => number == 1)[0])

    if(maxindex != expectedindex) wrong++
}

console.log('Total Wrong:', wrong)
console.log('Relative Wrong:', wrong/training.sets.length)