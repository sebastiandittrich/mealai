export const Architect: {
    Hopfield: any;
    LSTM: any;
    Liquid: any;
    Perceptron: any;
};
export const Layer: any;
export const Network: any;
export const Neuron: any;
export const Trainer: any;
